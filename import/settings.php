<?php
	$guildname        = 'Sign';
	$realmname        = 'Teldrassil';
	$guildfields      = 'members';      // Möglich sind: achievements, members
	$requestURL = 'http://eu.battle.net/api/wow/guild/'.rawurlencode($realmname).'/'.rawurlencode($guildname).'?fields='.$guildfields;

	function curl1()
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $requestURL);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		$response = curl_exec($ch);
		curl_close($ch);
		$inhalt = json_decode($response, true);
	}
?>