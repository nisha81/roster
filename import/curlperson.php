<?php

	$realm		= $member['character']['realm'];		   
    $charurl = 'http://eu.battle.net/api/wow/character/'.rawurlencode($realm).'/'.rawurlencode($name).'?fields=items';
    $profurl = 'http://eu.battle.net/api/wow/character/'.rawurlencode($realm).'/'.rawurlencode($name).'?fields=professions';

	// build the individual requests as above, but do not execute them
	$ch_1 = curl_init($charurl);
	$ch_2 = curl_init($profurl);
	curl_setopt($ch_1, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch_2, CURLOPT_RETURNTRANSFER, true);

	// build the multi-curl handle, adding both $ch
	$mh = curl_multi_init();
	curl_multi_add_handle($mh, $ch_1);
	curl_multi_add_handle($mh, $ch_2);

	// execute all queries simultaneously, and continue when all are complete
	$running = null;
	do {
	curl_multi_exec($mh, $running);
	} while ($running);

	// all of our requests are done, we can now access the results
	$response_1 = curl_multi_getcontent($ch_1);
	$response_2 = curl_multi_getcontent($ch_2);
	$char = json_decode($response_1, true);
	$prof = json_decode($response_2, true);

    @$ailvl 	= $char['items']['averageItemLevelEquipped'];
	@$prof1		= $prof['professions']['primary']['0']['name'];
	@$prof2		= $prof['professions']['primary']['1']['name'];
    $points 	= $member['character']['achievementPoints'];
    $tbnail		= $member['character']['thumbnail'];
    @$spec 		= $member['character']['spec']['name']; 
    @$specimg 	= $member['character']['spec']['icon'];  
    $race		= $member['character']['race'];
    $klasse 	= $member['character']['class'];   // class ist in einigen PHO Versionen reserviert, deshalb "klasse"

    $i++;

	// Felder der Tabelle			
	$fields = "`name`, `realm`, `rank`, `points`, `tbnail`, `race`, `ailvl`, `level`, `prof1`,  `prof2`, `spec`, `specimg`, `klasse`, `zeitstempel`";
	$values = " '".mysql_escape_string(convutf8($name))."',
	'".mysql_escape_string($realm)."',
	'".mysql_escape_string($rank)."',
	'".mysql_escape_string($points)."',
	'".mysql_escape_string($tbnail)."',
	'".mysql_escape_string($race)."',
	'".mysql_escape_string($ailvl)."',
	'".mysql_escape_string($level)."',
	'".mysql_escape_string($prof1)."',
	'".mysql_escape_string($prof2)."',
	'".mysql_escape_string($spec)."',
	'".mysql_escape_string($specimg)."',
	'".mysql_escape_string($klasse)."', now()";
	// Der Befehl > mysql_escape_string() < filtert Böhses und escaped ', \ sowie " Zeichen selbstständig
	
	// Und jetzt alles Speichern ...
	mysql_query("INSERT INTO ".$tabelle." (".$fields.") VALUES (".$values.") ") or die("<h3>Oooops, Fehler:</h3>".mysql_error());
?>